# ESET challenge solution

I created a small application that is capable of searching provided directories for N biggest files.

The app was developed with use of InteliJ Idea IDE, android.materials library and greerobot.EventBus for event handling.

The solution uses the SearchService for search processing and SearchJob is run on a separate thread
to avoid blocking the main one. If further developed, the application could run multiple threads
for different physical memories (I didn't implement this solution, but it should be rather simple,
I see complication only in determining what memory source a URI belong to).

The **APK** file is located in the app/debug/ folder. 