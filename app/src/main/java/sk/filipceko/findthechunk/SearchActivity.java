package sk.filipceko.findthechunk;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import sk.filipceko.findthechunk.search.SearchService;

/**
 * Activity launching SearchService and displaying search icon to the user.
 */
public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        startSearch();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Launches {@link SearchService}
     */
    public void startSearch() {
        Intent intent = new Intent(this, SearchService.class);
        intent.putExtras(getIntent().getExtras());
        startService(intent);
    }

    /**
     * {@link SearchService} result event handling method
     * @param event to handle, it caries the result data
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResult(SearchService.SearchFinishedEvent event) {
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putParcelableArrayListExtra(ResultsActivity.RESULT_LIST_KEY, event.getResult());
        startActivity(intent);
        finish();
    }
}