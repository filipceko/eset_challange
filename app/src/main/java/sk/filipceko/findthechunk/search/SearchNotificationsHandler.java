package sk.filipceko.findthechunk.search;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import sk.filipceko.findthechunk.R;
import sk.filipceko.findthechunk.ResultsActivity;
import sk.filipceko.findthechunk.SearchActivity;

import java.util.ArrayList;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Class handling Search notifications
 */
public class SearchNotificationsHandler {

    public static final int SEARCHING_NOTIFICATION_ID = 154;
    public static final int FINISHED_NOTIFICATION_ID = 157;
    private static final String NOTIFICATION_CHANNEL_ID = "filipceko.findTheChunk.searchNotification";

    private final NotificationManager notificationManager;

    public SearchNotificationsHandler(Context context) {
        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        createNotificationChannel(context);
    }

    /**
     * Produces notification signaling the search is in progress.
     * @param context context of the notification
     */
    public void showSearchingNotification(Context context) {
        Resources resources = context.getResources();
        CharSequence text = resources.getText(R.string.searching_notification_text);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, SearchActivity.class), 0);
        Notification notification = new Notification.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_search_in_progress)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(resources.getText(R.string.search_in_progress))
                .setContentText(text)
                .setContentIntent(contentIntent)
                .build();
        notificationManager.notify(SEARCHING_NOTIFICATION_ID, notification);
    }

    /**
     * Hides Search in progress notification
     */
    public void hideSearchingNotification() {
        notificationManager.cancel(SEARCHING_NOTIFICATION_ID);
    }

    /**
     * Produces notification signaling the search has finished.
     * @param context context of the notification
     * @param results results of the search
     */
    public void showFinishedNotification(Context context, ArrayList<Uri> results) {
        Resources resources = context.getResources();
        CharSequence text = resources.getText(R.string.finished_notification_text);
        Intent intent = new Intent(context, ResultsActivity.class);
        intent.putParcelableArrayListExtra(ResultsActivity.RESULT_LIST_KEY, results);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_search_done)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(resources.getText(R.string.app_name))
                .setContentText(text)
                .setContentIntent(contentIntent)
                .build();
        notificationManager.notify(FINISHED_NOTIFICATION_ID, notification);
    }

    /**
     * Hides Search finished notification
     */
    public void hideFinishedNotification() {
        notificationManager.cancel(FINISHED_NOTIFICATION_ID);
    }

    /**
     * Creates notification channel for the search signalling notifications
     * @param context context of the request
     */
    private void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
