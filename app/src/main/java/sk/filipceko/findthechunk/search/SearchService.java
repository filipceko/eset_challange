package sk.filipceko.findthechunk.search;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.os.HandlerCompat;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service handling asynchronous search for big files
 */
public class SearchService extends Service {

    public static final String FOLDER_LIST_KEY = "filipceko.findTheChunk.folderListKey";
    public static final String N_NUMBER_KEY = "filipceko.findTheChunk.nNumberKey";

    /** jobs in progress searching for files **/
    private final Set<SearchJob> runningJobs = new HashSet<>();
    private SearchNotificationsHandler notificationsHandler;
    /** Final result merged from all running threads **/
    private SearchResultCollector resultMerger;

    @Override
    public void onCreate() {
        notificationsHandler = new SearchNotificationsHandler(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        List<Uri> folderList = intent.getParcelableArrayListExtra(FOLDER_LIST_KEY);
        int filesToFind = intent.getIntExtra(N_NUMBER_KEY, 1);
        resultMerger = new SearchResultCollector(filesToFind);
        notificationsHandler.showSearchingNotification(this);
        searchForChunks(folderList, filesToFind);
        Log.i("SearchService", "Search initiated");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        notificationsHandler.hideSearchingNotification();
    }

    @Override
    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Fires new Thread searching for big files in given directories
     * @param uriList list of directories to be searched, represented by URI
     * @param filesToFind number of biggest files to search for
     */
    public void searchForChunks(List<Uri> uriList, int filesToFind) {
        Handler handler = HandlerCompat.createAsync(Looper.myLooper());
        SearchJob job = new SearchJob(this, uriList, handler, filesToFind);
        Thread workerThread = new Thread(job);
        runningJobs.add(job);
        workerThread.start();
    }

    /**
     * Handles search job result collection
     * @param job finished SearchJob
     */
    public void onSearchJobDone(@NonNull SearchJob job) {
        resultMerger.includeResult(job.getResult());
        runningJobs.remove(job);
        if (runningJobs.isEmpty()) {
            //All jobs done, wrap up and fire event
            notificationsHandler.hideSearchingNotification();
            ArrayList<Uri> results = resultMerger.getResults();
            notificationsHandler.showFinishedNotification(this, results);
            EventBus.getDefault().post(new SearchFinishedEvent(results));
            stopSelf();
        }
    }

    /**
     * Event class propagating that the search was finished
     */
    public static class SearchFinishedEvent {
        private final ArrayList<Uri> result;

        public SearchFinishedEvent(ArrayList<Uri> result) {
            this.result = result;
        }

        public ArrayList<Uri> getResult() {
            return result;
        }
    }
}
