package sk.filipceko.findthechunk.search;

import android.net.Uri;
import androidx.documentfile.provider.DocumentFile;

import java.util.ArrayList;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Search result holder class
 */
public class SearchResultCollector {
    /**
     * Number of files to collect
     **/
    private final int filesToFind;
    /**
     * Biggest currenylu discovered files sorted by size
     **/
    TreeSet<ResultEntry> selectedFiles = new TreeSet<>();

    public SearchResultCollector(int filesToFind) {
        this.filesToFind = filesToFind;
    }

    public ArrayList<Uri> getResults() {
        return selectedFiles.stream().map(resultEntry -> resultEntry.fileUri)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Add file to the result collection if fulfills contract
     * @param file file to be added
     * @return true is was added, false otherwise
     */
    public boolean addFile(DocumentFile file) {
        if (filesToFind <= 0) {
            return false;
        }
        if (selectedFiles.size() < filesToFind) {
            return selectedFiles.add(new ResultEntry(file));
        } else if (selectedFiles.first().fileSize < file.length()) {
            selectedFiles.remove(selectedFiles.first());
            selectedFiles.add(new ResultEntry(file));
            return true;
        }
        return false;
    }

    /**
     * Size of the smallest file currently collected or -1 if structure isn't full
     * @return smallest file size or -1
     */
    public long getSmallestSize() {
        if (selectedFiles.size() < filesToFind || filesToFind <= 0) {
            return -1L;
        } else {
            return selectedFiles.first().fileSize;
        }
    }

    /**
     * Merges other result into this
     * @param otherCollector collector to copy into this one
     */
    public void includeResult(SearchResultCollector otherCollector) {
        if (filesToFind <= 0) {
            return;
        }
        NavigableSet<ResultEntry> otherResult = otherCollector.selectedFiles.descendingSet();
        for (ResultEntry resultEntry : otherResult) {
            if (this.selectedFiles.size() < this.filesToFind) {
                selectedFiles.add(resultEntry);
            } else if (selectedFiles.first().fileSize < resultEntry.fileSize) {
                selectedFiles.remove(selectedFiles.first());
                selectedFiles.add(resultEntry);
            } else {
                return;
            }
        }
    }

    /**
     * Single file representation
     */
    private static class ResultEntry implements Comparable<ResultEntry> {
        private final Uri fileUri;
        private final long fileSize;

        public ResultEntry(DocumentFile file) {
            this.fileUri = file.getUri();
            this.fileSize = file.length();
        }

        @Override
        public int compareTo(ResultEntry other) {
            return Long.compare(this.fileSize, other.fileSize);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ResultEntry that = (ResultEntry) o;
            return fileUri.equals(that.fileUri);
        }

        @Override
        public int hashCode() {
            return fileUri.hashCode();
        }
    }
}
