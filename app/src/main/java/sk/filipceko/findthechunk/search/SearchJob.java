package sk.filipceko.findthechunk.search;

import android.net.Uri;
import android.os.Handler;
import androidx.documentfile.provider.DocumentFile;

import java.util.List;

/**
 * Runnable to be executed to search for biggest files in a given set of folders represented by their URI.
 * When finished, hands over data to the {@link SearchService} via provided handler.
 */
class SearchJob implements Runnable {

    /** SearchService handling the result **/
    private final SearchService searchService;
    /** result collecting structure */
    private final SearchResultCollector resultCollector;
    private final List<Uri> urisToSearch;
    /** Handler to handle result collection **/
    private final Handler resultHandler;

    /**
     * Public constructor
     * @param searchService service handling result
     * @param urisToSearch list of URIs to be searched for files
     * @param resultHandler handler of the result processing thread
     * @param filesToFind number of files to search for
     */
    SearchJob(SearchService searchService, List<Uri> urisToSearch, Handler resultHandler, int filesToFind) {
        this.searchService = searchService;
        this.urisToSearch = urisToSearch;
        this.resultHandler = resultHandler;
        this.resultCollector = new SearchResultCollector(filesToFind);
    }

    @Override
    public void run() {
        for (Uri uri : urisToSearch) {
            DocumentFile file = DocumentFile.fromTreeUri(searchService, uri);
            if (file != null) {
                searchSubTree(file);
            }
        }
        resultHandler.post(() -> searchService.onSearchJobDone(this));
    }

    /**
     * Method that searches recursively trough the File tree in search for for big files
     * @param documentFile root of the local tree
     */
    private void searchSubTree(DocumentFile documentFile) {
        if (documentFile.isDirectory()) {
            for (DocumentFile childFile : documentFile.listFiles()) {
                searchSubTree(childFile);
            }
        } else {
            if (resultCollector.getSmallestSize() < documentFile.length()) {
                resultCollector.addFile(documentFile);
            }
        }
    }

    public SearchResultCollector getResult() {
        return resultCollector;
    }
}
