package sk.filipceko.findthechunk;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import sk.filipceko.findthechunk.search.SearchService;
import sk.filipceko.findthechunk.recyclerAdapters.DirectoryRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * Main activity
 * Handles UI allowing user to select directories for search
 */
public class DirectorySelectionActivity extends AppCompatActivity {

    private static final int FOLDER_REQUEST_CODE = 4568;

    /** Directories selected by the user **/
    private final ArrayList<Uri> directories = new ArrayList<>();
    RecyclerView.Adapter<?> adapter;
    TextInputEditText nTextInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Recycler View setup
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        adapter = new DirectoryRecyclerViewAdapter(directories, this::removeUri);
        recyclerView.setAdapter(adapter);
        //Search Button setup
        MaterialButton searchButton = findViewById(R.id.search_button);
        searchButton.setOnClickListener(this::search);
        //Add directory FAB setup
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Choose a directory to add"), FOLDER_REQUEST_CODE);
        });
        nTextInput = findViewById(R.id.n_text_input);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FOLDER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri folderUri = data.getData();
                Log.i("Test", "Selected Folder: " + folderUri.toString());
                directories.add(folderUri);
                adapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * on Search button click method launching {@link SearchActivity}
     * @param view of the button pressed
     */
    public void search(View view) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putParcelableArrayListExtra(SearchService.FOLDER_LIST_KEY, directories);
        String nText = nTextInput.getText().toString();
        int n = nText.isEmpty() ? 1 : Integer.parseInt(nText);
        intent.putExtra(SearchService.N_NUMBER_KEY, n);
        startActivity(intent);
    }

    /**
     * Method that removes already selected folder from the list
     * Used as onClickMethod for the recycler
     * @param uriToRemove selected folder's uri
     */
    public void removeUri(Uri uriToRemove) {
        directories.remove(uriToRemove);
        adapter.notifyDataSetChanged();
    }
}