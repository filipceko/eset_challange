package sk.filipceko.findthechunk.recyclerAdapters;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import sk.filipceko.findthechunk.R;

import java.util.List;

/**
 * UriAdapter for directories with removal functionality
 */
public class DirectoryRecyclerViewAdapter extends UriRecyclerViewAdapter<DirectoryRecyclerViewAdapter.ViewHolder> {

    /** Listener to remove button of the items **/
    OnRemoveButtonListener onRemoveButtonListener;

    public DirectoryRecyclerViewAdapter(List<Uri> uris, OnRemoveButtonListener onRemoveListener) {
        super(uris);
        this.onRemoveButtonListener = onRemoveListener;
    }

    @Override
    public @NonNull ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_recycler_directory, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Uri uri = uriList.get(position);
        holder.nameTextView.setText(uri.getLastPathSegment());
        holder.addressTextView.setText(uri.toString());
        holder.removeButton.setOnClickListener((view) -> onRemoveButtonListener.onRemoveButtonPress(uri));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView nameTextView;
        public final TextView addressTextView;
        public final MaterialButton removeButton;

        public ViewHolder(View view) {
            super(view);
            nameTextView = view.findViewById(R.id.name_text_view);
            addressTextView = view.findViewById(R.id.address_text_view);
            removeButton = view.findViewById(R.id.remove_button);
        }
    }

    /**
     * Called when an item's remove button is pressed
     */
    public interface OnRemoveButtonListener {
        void onRemoveButtonPress(Uri uriToRemove);
    }
}