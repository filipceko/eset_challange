package sk.filipceko.findthechunk.recyclerAdapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.recyclerview.widget.RecyclerView;
import sk.filipceko.findthechunk.R;

import java.util.List;

/**
 * UriAdapter for found files
 */
public class FileRecyclerViewAdapter extends UriRecyclerViewAdapter<FileRecyclerViewAdapter.ViewHolder> {

    public FileRecyclerViewAdapter(List<Uri> uris) {
        super(uris);
    }

    @Override
    public @NonNull
    ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_recycler_file, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Uri uri = uriList.get(position);
        holder.uri = uri;
        DocumentFile documentFile = DocumentFile.fromSingleUri(holder.context, uri);
        if (documentFile == null || !documentFile.exists()) {
            Log.e("ResultLoader", "Document at " + uri.getPath() + " failed to load");
            return;
        }
        holder.itemNumberTextView.setText(Integer.toString(position + 1));
        holder.addressTextView.setText(uri.getPath());
        holder.nameTextView.setText(documentFile.getName());
        holder.sizeTextView.setText(documentFile.length() + " B");
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView itemNumberTextView;
        public final TextView nameTextView;
        public final TextView addressTextView;
        public final TextView sizeTextView;
        public final Context context;
        public Uri uri;

        public ViewHolder(View view) {
            super(view);
            context = view.getContext();
            itemNumberTextView = view.findViewById(R.id.item_number);
            nameTextView = view.findViewById(R.id.file_name);
            addressTextView = view.findViewById(R.id.file_address);
            sizeTextView = view.findViewById(R.id.file_size);
        }
    }
}
