package sk.filipceko.findthechunk.recyclerAdapters;

import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Class handling common Uri adapter implementation
 * @param <T> ViewHolder
 */
public abstract class UriRecyclerViewAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    protected final List<Uri> uriList;

    public UriRecyclerViewAdapter(List<Uri> items) {
        uriList = items;
    }

    @Override
    public int getItemCount() {
        return uriList.size();
    }
}
