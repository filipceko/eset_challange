package sk.filipceko.findthechunk;

import android.app.NotificationManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import sk.filipceko.findthechunk.search.SearchNotificationsHandler;
import sk.filipceko.findthechunk.recyclerAdapters.FileRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Activity presenting the results to the user
 */
public class ResultsActivity extends AppCompatActivity {

    public static final String RESULT_LIST_KEY = "filipceko.findTheChunk.resultListKey";
    RecyclerView.Adapter<?> adapter;
    private ArrayList<Uri> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        results = intent.getParcelableArrayListExtra(RESULT_LIST_KEY);
        Collections.reverse(results); // Reverse to display from biggest to smallest file
        setContentView(R.layout.activity_results);
        closeFinishNotification();
        displayResults();
    }

    /**
     * Closes Search Finished notification produced by the {@link sk.filipceko.findthechunk.search.SearchService}
     */
    private void closeFinishNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(SearchNotificationsHandler.FINISHED_NOTIFICATION_ID);
    }

    /**
     * Initializes the Recycler View and provides result data
     */
    private void displayResults() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        adapter = new FileRecyclerViewAdapter(results);
        recyclerView.setAdapter(adapter);
        TextView textView = findViewById(R.id.hintText);
        if (results.isEmpty()) {
            textView.setText(getResources().getString(R.string.no_files_label));
        } else {
            textView.setText(getResources().getString(R.string.number_of_results_label, results.size()));
        }
    }
}